defmodule TitanWeb.Application do
  # See https://hexdocs.pm/elixir/Application.html
  # for more information on OTP Applications
  @moduledoc false

  use Application

  @impl true
  def start(_type, _args) do
    children = [
      # Start the Telemetry supervisor
      TitanWeb.Telemetry,
      # Start the Endpoint (http/https)
      TitanWeb.Endpoint
      # Start a worker by calling: TitanWeb.Worker.start_link(arg)
      # {TitanWeb.Worker, arg}
    ]

    # See https://hexdocs.pm/elixir/Supervisor.html
    # for other strategies and supported options
    opts = [strategy: :one_for_one, name: TitanWeb.Supervisor]
    Supervisor.start_link(children, opts)
  end

  # Tell Phoenix to update the endpoint configuration
  # whenever the application is updated.
  @impl true
  def config_change(changed, _new, removed) do
    TitanWeb.Endpoint.config_change(changed, removed)
    :ok
  end
end
