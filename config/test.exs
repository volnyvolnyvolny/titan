import Config

# Configure your database
#
# The MIX_TEST_PARTITION environment variable can be used
# to provide built-in test partitioning in CI environment.
# Run `mix help test` for more information.
config :titan, Titan.Repo,
  username: "postgres",
  password: "postgres",
  hostname: "localhost",
  database: "titan_test#{System.get_env("MIX_TEST_PARTITION")}",
  pool: Ecto.Adapters.SQL.Sandbox,
  pool_size: 10

# We don't run a server during test. If one is required,
# you can enable the server option below.
config :titan_web, TitanWeb.Endpoint,
  http: [ip: {127, 0, 0, 1}, port: 4002],
  secret_key_base: "pm4DAvNX4jTTXh8GTop24gYNyQ0753LHbxFNBt6a5+Na2KB1ZENC/bsyyMGyRkqt",
  server: false

# Print only warnings and errors during test
config :logger, level: :warn

# In test we don't send emails.
config :titan, Titan.Mailer, adapter: Swoosh.Adapters.Test

# Initialize plugs at runtime for faster test compilation
config :phoenix, :plug_init_mode, :runtime
